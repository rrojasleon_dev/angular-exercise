import { Component, OnInit } from '@angular/core';
import { RestService } from './services/rest.service';
import { map, filter } from 'rxjs/operators';
import { s } from '@angular/core/src/render3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(private restService: RestService) {}

  title = 'app';
  data: any = {};
  dataReady: boolean = false;
  filteredData: any = {};
  keyword: string = '';

  ngOnInit() {
    this.restService.fetchData().subscribe(
      // on data fetch
      (data) => {
        this.data = data;
      },
      // on error
      err => {},
      // on end
      () => {
        this.dataReady = true;
        this.filteredData = this.data;
      }
    );
  }

  private search() {
    if(this.keyword === '') {
      this.filteredData = this.data;
    } else {
      let itemsFound = this.data.filter(item => {
        return item.title.includes(this.keyword)
      });
      this.filteredData = itemsFound;
    }
  }
}
