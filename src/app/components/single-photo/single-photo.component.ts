import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ag-single-photo',
  templateUrl: './single-photo.component.html'
})
export class SinglePhotoComponent implements OnInit {
  @Input() item: any;

  constructor() { }

  ngOnInit() {
  }

}
