// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { SinglePhotoComponent } from './components/single-photo/single-photo.component';

// Services
import { RestService } from './services/rest.service';

@NgModule({
  declarations: [
    AppComponent,
    SinglePhotoComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    RestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
