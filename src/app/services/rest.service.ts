import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RestService {
    private endpoint = 'https://jsonplaceholder.typicode.com/albums/1/photos?albumId=1';

    constructor(private http: HttpClient) {}

    fetchData() {
        return this.http.get(this.endpoint);
    }
}